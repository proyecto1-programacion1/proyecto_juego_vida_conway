# !/usr/bin/env python3
# ! -*- coding:utf-8- -*-


import random
import time
import os
print('imprimiendo de inmediato')

# la base de todo
base = []
# tamaño filas columnas
a = 12

comprobador_de_vida = True

# restringe la busqueda a dendro de la lista sobre lista
# el valor serà cuando se encuentre dentro de la matriz
# y es 0 cuendo se encuentre fuera


def si_se_pasa(base, i, j):
    limite = 1
    if i < 0:
        limite = 0
    elif j < 0:
        limite = 0
    elif i == a:
        limite = 0
    elif j == a:
        limite = 0
    return limite

# función imprimir


def imprimir(matrix):
    for i in range(a):
        for j in range(a):
            print(matrix[i][j], end=' ')
        print()
# funciones contadoras


def cuenta_vivas(matrix):
    contador_vivas = 0
    for i in range(a):
        for j in range(a):
            if matrix[i][j] == 'O':
                contador_vivas += 1
    return contador_vivas


def cuenta_muertas(matrix):
    contador_muertas = 0
    for i in range(a):
        for j in range(a):
            if matrix[i][j] == '*':
                contador_muertas += 1
    print('celulas muertas: ', contador_muertas)
# función que reccorre las 8 celulas vecinas de la viva que encuentre


def cuenta_vecinas(matrix, i, j):
    contador_vecinas = 0
    if si_se_pasa(matrix, i-1, j-1) == 1 and matrix[i-1][j-1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i-1, j) == 1 and matrix[i-1][j] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i-1, j+1) == 1 and matrix[i-1][j+1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i, j-1) == 1 and matrix[i][j-1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i, j+1) == 1 and matrix[i][j+1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i+1, j-1) == 1 and matrix[i+1][j-1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i+1, j) == 1 and matrix[i+1][j] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i+1, j+1) == 1 and matrix[i+1][j+1] == 'O':
        contador_vecinas += 1
    return contador_vecinas
# basandose en las reglas del juego, mata o revive a las celulas


def cambio(base, i, j):
    if base[i][j] == 'O':
        if cuenta_vecinas(base, i, j) < 2 or cuenta_vecinas(base, i, j) > 3:
            base[i][j] = '*'
    if base[i][j] == '*':
        if cuenta_vecinas(base, i, j) == 3:
            base[i][j] = 'O'
# agrega las columnas, ademas de simbolos representativos del juego,
# crea las posiciones aleatorias de los simbolos y agrega los elementos a
# las columnas


for i in range(a):
    base.append([])
    for j in range(a):
        celulas = ['*', 'O']
        celulas_random = random.randint(0, 1)
        base[i].append(celulas[celulas_random])

while comprobador_de_vida:
    imprimir(base)
    print('celulas vivas: ', cuenta_vivas(base))
    cuenta_muertas(base)
    for i in range(a):
        for j in range(a):
            cambio(base, i, j)
    if cuenta_vivas(base) == 0:
        comprobador_de_vida = False
    time.sleep(2.4)
    os.system('clear')
    # 'clear' en ubuntu
print('fin del juego')
