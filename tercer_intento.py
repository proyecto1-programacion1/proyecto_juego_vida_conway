#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
import random
import time
import os
print('imrprimiendo de inmediato')

base = [] # la base de todo
a = 10 # tamaÃ±o filas columnas
n = 0 # solo es para probar la eficacia de las funciones de recorrido
J = True

# restringe la busqueda a dendro de la lista sobre lista
def si_se_pasa(base, i, j):
    point = 1
    if i < 0:
        point = 0
    elif j < 0:
        point = 0
    elif i == a:
        point = 0
    elif j == a:
        point = 0
    return point
# funciÃ³n imprimir    
def imprimir(matrix):
    for i in range(a):
        for j in range(a):        
            print(matrix[i][j],end=' ')
        print()
# funciones contadoras
def cuenta(matrix):
    contador_vivas = 0
    for i in range(a):
        for j in range(a):        
            if matrix[i][j] == 'O':
                contador_vivas += 1
    return contador_vivas
def cuenta_muertas(matrix):
    contador_muertas = 0
    for i in range(a):
        for j in range(a):        
            if matrix[i][j] == '*':
                contador_muertas += 1
    print('celulas muertas: ', contador_muertas)
#funciÃ³n que reccorre las 8 celulas vecinas de la viva que encuentre
def recorre(matrix, i, j):         
    contador_vecinas = 0
    if si_se_pasa(matrix, i-1, j-1) == 1 and matrix[i-1][j-1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i-1, j) == 1 and matrix[i-1][j] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i-1, j+1) == 1 and matrix[i-1][j+1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i, j-1) == 1 and matrix[i][j-1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i, j+1) == 1 and matrix[i][j+1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i+1, j-1) == 1 and matrix[i+1][j-1] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i+1, j) == 1 and matrix[i+1][j] == 'O':
        contador_vecinas += 1
    if si_se_pasa(matrix, i+1, j+1) == 1 and matrix[i+1][j+1] == 'O':
        contador_vecinas += 1
    return contador_vecinas
# basandose en las reglas del juego, mata o revive a las celulas
def cambio(base, i, j):
    if base[i][j] == 'O':
        if recorre(base, i, j) < 2 or recorre(base, i, j) > 3:
            base[i][j] = '*'
    if base[i][j] == '*':
        if recorre(base, i, j) == 3:
            base[i][j] = 'O'

for i in range(a):
    base.append([]) # agrega las columnas 
    for j in range(a):
        celulas = ['*', 'O'] # simbolos representativos
        # crear posiciones aleatorias de los simbolos
        celulas_random = random.randint(0,1) 
        # agrega elementos a las columnas
        base[i].append(celulas[celulas_random])

while J == True:
    imprimir(base)
    print('celuas vivas: ', cuenta(base))
    cuenta_muertas(base)
    for i in range(a):
        for j in range(a):
            cambio(base, i, j)
    if cuenta(base) == 0:
        J = False
    time.sleep (2.4)
    os.system('cls')
    # 'clear' en ubuntu
print('fin del juego')    